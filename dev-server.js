const express = require('express');
const app = express();


const port = '3003';
const staticDir = '/dist';

app.use(express.static(`${__dirname}${staticDir}`));
// route requests for static files to appropriate directory
app.use('/dist', express.static(`${__dirname}${staticDir}`));
// final catch-all route to index.html defined last
app.get('/*', (req, res) => {
  res.sendFile(`${__dirname}${staticDir}/templates/index.html`);
});

app.listen(port);
console.log(`working on ${port}`);
