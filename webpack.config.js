const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');


const srcRoot = `./src`;
const srcEntry = `${srcRoot}/index.ts`;
const distRoot = `./dist`;
const distRootJs = `${distRoot}/js`;

module.exports = {
  entry: {
    'index.js': `${srcEntry}`,
  },
  output: {
    path: path.resolve(__dirname, distRootJs),
    filename: '[name]',
    libraryTarget: 'umd'
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        exclude: /node_modules/,
        use: {
          loader: 'ts-loader',
        }
      },
    ],
  },
  resolve: {
    extensions: ['.ts', '.js']
  },
  plugins: [
    new CleanWebpackPlugin([distRoot], { root: path.resolve(__dirname, '.') }),
    new CopyWebpackPlugin([{ from: `${srcRoot}/index.html`, to: path.resolve(__dirname, distRoot, 'templates') }])
  ]
};
