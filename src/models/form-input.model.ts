export interface FormInputModel {
  label: string;
  name: string;
  value: any;
  type: string;
}
