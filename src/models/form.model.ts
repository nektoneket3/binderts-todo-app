import { Observable } from 'rxjs';

import { FormInputModel } from './form-input.model';


export type FormModel = Observable<FormInputModel>[];
