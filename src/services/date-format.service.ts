import { injectable } from 'binderts';


@injectable({ shared: true })
export class DateFormatService {

  getDate(date: Date): string {
    return `${date.toDateString()} at ${this.offset(date.getHours())}:${this.offset(date.getMinutes())}:${this.offset(date.getSeconds())}`;
  }

  offset(value: number): string  {
    return value < 10 ? `0${value}`: `${value}`;
  }
}
