import { Observable, of, Subject } from 'rxjs';
import { distinctUntilKeyChanged, map } from 'rxjs/operators';

import { TodoModel } from '../models/todo.model';
import { FormModel } from '../models/form.model';


export class TodoFormCreatorService {

  create(src: Subject<TodoModel>): Observable<FormModel> {
    return of([
      src.pipe(
        distinctUntilKeyChanged('description'),
        map(({ description }) => ({
          name: 'description',
          label: 'Description',
          value: description,
          type: 'text'
        }))
      ),
      src.pipe(
        distinctUntilKeyChanged('done'),
        map(({ done }) => ({
          name: 'done',
          label: 'Is done',
          value: done,
          type: 'checkbox'
        }))
      )
    ]);
  }
}
