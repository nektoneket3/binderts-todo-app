import { bootstrapApp } from 'binderts';

import { TodoContainerComponent } from './components/todo-container.component';
import { FormComponent } from './components/form.component';
import { TableComponent } from './components/table.component';
import { TableCaptionComponent } from './components/table-caption.component';
import { TableRowComponent } from './components/table-row.component';
import { EditableTableRowComponent } from './components/editable-table-row.component';
import { ButtonComponent } from './components/button.component';
import { InputComponent } from './components/input.component';
import { BadgeComponent } from './components/badge.component';

import { store } from './store'
import { DateFormatService } from './services/date-format.service';
import { TodoFormCreatorService } from './services/todo-form-creator.service';


bootstrapApp({
  root: TodoContainerComponent,
  components: [
    TodoContainerComponent,
    FormComponent,
    TableComponent,
    TableCaptionComponent,
    TableRowComponent,
    EditableTableRowComponent,
    ButtonComponent,
    InputComponent,
    BadgeComponent
  ],
  services: {
    DateFormatService,
    TodoFormCreatorService
  },
  store
});
