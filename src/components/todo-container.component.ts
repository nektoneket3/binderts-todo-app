import { combineLatest, of } from 'rxjs';
import { map, switchMap, withLatestFrom } from 'rxjs/operators';
import { Bindable, getBindableDecorators, component, Store, STORE, SUBSCRIPTIONS, BindableSubscriptions } from 'binderts';

import { State } from '../store';
import { changeTodo, createTodo, changeMode, updateTodo, removeTodos } from '../store/mutations';
import { TodoFormCreatorService } from '../services/todo-form-creator.service';
import { TodoModel } from '../models/todo.model';
import { FormModel } from '../models/form.model';
import { FormInputModel } from '../models/form-input.model';


type Count = number;
type Condition = boolean;

const { bindable, subscribedBindable } = getBindableDecorators<TodoContainerComponent>();

@component({
  name: 'todo-root',
  template: template(),
  style: style(),
  dependencies: [STORE, SUBSCRIPTIONS, 'TodoFormCreatorService']
})
export class TodoContainerComponent {

  @bindable<Condition>(({ store }) => store.state.darkMode)
  isDarkMode!: Bindable<Condition>;

  @bindable<Bindable<TodoModel>[]>(({ store }) => store.state.todos)
  todoList!: Bindable<Bindable<TodoModel>[]>;

  @bindable<TodoModel[]>((self) => self.todoList.pipe(
    switchMap(todoList => !!todoList.length ? combineLatest(todoList) : of([])),
    map(todoList => todoList.filter(({ removed }) => !removed)),
  ))
  todoDisplayedList!: Bindable<TodoModel[]>;

  @bindable<Count>((self) => self.todoDisplayedList.pipe(
    map(todoList => todoList.filter(({ removed }) => !removed).length),
  ))
  todoListTotal!: Bindable<Count>;

  @bindable<Count>((self) => self.todoDisplayedList.pipe(
    map(todoList => todoList.filter(({ done }) => done).length),
  ))
  todoListDone!: Bindable<Count>;

  @bindable<Count>((self) => self.todoDisplayedList.pipe(
    withLatestFrom(self.todoListTotal, self.todoListDone),
    map(([, total, done]) => total - done),
  ))
  todoListLeft!: Bindable<Count>;

  @bindable<FormModel>(self => self.todoFormCreator.create(self.store.state.todo))
  todoForm!: Bindable<FormModel>;

  @subscribedBindable<FormInputModel>(
    ({ name, value }, { store }) => store.dispatch.mutation(changeTodo, { [name]: value })
  )
  todoFormChanged!: Bindable<FormInputModel>;

  @subscribedBindable((e, { store }) => store.dispatch.mutation(createTodo))
  createTodoButtonClicked!: Bindable<Event>;

  @subscribedBindable((e, { store }) => store.dispatch.mutation(removeTodos))
  removeAllButtonClicked!: Bindable<Event>;

  @subscribedBindable((e, { store }) => store.dispatch.mutation(changeMode))
  changeModeClicked!: Bindable<Event>;

  @subscribedBindable((updatedTodo, { store }) => store.dispatch.mutation(updateTodo, updatedTodo))
  todoUpdated!: Bindable<[number, TodoModel]>;

  constructor(
    private store: Store<State>,
    private subscriptions: BindableSubscriptions,
    private todoFormCreator: TodoFormCreatorService,
  ) {}
}

function template(): string {
  return `
    <div class="container"
         data-bind-class="{ 'dark': 'isDarkMode' }">

      <todo-button class="top-button"
                   data-bind-label="'Click to change background'"
                   data-bind-clicked="changeModeClicked">
      </todo-button>

      <todo-table-caption data-bind-count="todoListTotal"></todo-table-caption>

      <div class="table-placeholder"
           data-bind-if="!todoListTotal">
        Nothing to show
      </div>

      <div class="table-section"
           data-bind-if="todoListTotal">
        <div class="side-area"></div>
        <todo-table class="center-area"
                    data-bind-rows="todoList"
                    data-bind-updated="todoUpdated">
        </todo-table>
        <div class="counters side-area">
            <div>
              Done: <todo-badge data-bind-count="todoListDone"></todo-badge>
            </div>
            <div>
              Left: <todo-badge data-bind-add-class="'red'"
                                data-bind-count="todoListLeft">
                    </todo-badge>
            </div>
          </div>
      </div>

      <todo-form data-bind-model="todoForm"
                 data-bind-changed="todoFormChanged">
      </todo-form>
  
      <todo-button data-bind-label="'Create'"
                   data-bind-clicked="createTodoButtonClicked">
      </todo-button>
      
      <todo-button data-bind-if="todoListTotal"
                   data-bind-label="'Remove all'"
                   data-bind-clicked="removeAllButtonClicked">
      </todo-button>
    </div>
  `;
}

function style(): string {
  return `
    <style>
      .container {
        display: flex;
        flex-direction: column;
        align-items: center;
        height: 100%;
      }
      .top-button {
        margin-top: 25px
      }
      .table-section {
        display: flex;
        max-height: 50%
      }
      .side-area {
        width: 15%
      }
      .center-area {
        width: 70%
      }
      .counters {
        display: flex;
        flex-direction: column;
      }
      .counters div {
        margin: 25px 15px 0 15px;
      }
      todo-form {
        margin-top: 15px;
      }
      todo-button {
        margin-top: 15px;
      }
      .dark {
        background-color: #e1e1e1;
      }
      .table-placeholder {
        margin: 20px 0 10px 0;
      }
    </style>
  `;
}
