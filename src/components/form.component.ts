import { component, input, Input, output, Output } from 'binderts';

import { FormModel } from '../models/form.model';
import { FormInputModel } from '../models/form-input.model';


@component({
  name: 'todo-form',
  template: template(),
  style: style()
})
export class FormComponent {

  @input() model!: Input<FormModel>;

  @output() changed!: Output<FormInputModel>;
}

function template(): string {
  return `
    <form>
      <label data-bind-repeat="input in model">
        <span class="label"><span data-bind-value="input.label"></span>:</span>
        <todo-input data-bind-model="input"
                    data-bind-changed="changed">
        </todo-input>
      </label>
      <br>
    </form>
  `;
}

function style(): string {
  return `
    <style>
      form {
        display: flex;
      }
      label {
        display: flex;
        align-items: center;
        margin-right: 10px;
      }
      todo-input {
        margin-left: 5px;
      }
      @media screen and (max-width: 1000px) {
        :host([no-label]) .label {
          display: none;
        }
      }
    </style>
  `;
}
