import { map } from 'rxjs/operators';
import { Bindable, getBindableDecorators, input, Input, component, Output, output } from 'binderts';

import { cellStyle } from './table.component';
import { DateFormatService } from '../services/date-format.service';
import { TodoModel } from '../models/todo.model';


interface TodoRowModel {
  description: TodoModel['description'];
  done: 'yes' | 'no',
  date: string,
}

const { bindable, subscribedBindable } = getBindableDecorators<TableRowComponent>();

@component({
  name: 'todo-table-row',
  template: template(),
  style: style(),
  dependencies: ['DateFormatService']
})
export class TableRowComponent {

  @input() model!: Input<TodoModel>;

  @output() clicked!: Output<TodoModel>;

  @bindable<TodoRowModel>((self) => self.model.pipe(
    map(({ description, done, date }) => ({
      description,
      done: done ? 'yes' : 'no',
      date: date ? self.dateFormatService.getDate(date) : ''
    }))
  ))
  row!: Bindable<TodoRowModel>;

  @subscribedBindable<[Event, TodoModel]>(
    self => self.model,
    ([, model], self) => self.clicked.next(model)
  )
  rowClicked!: Bindable<[Event, TodoModel]>;

  constructor(
    private dateFormatService: DateFormatService
  ) {}
}

function template(): string {
  return `
    <div class="row"
         data-bind-click="rowClicked">
      <div class="cell">
        <p data-bind-value="row.description"></p>
      </div>
      <div class="cell">
        <p data-bind-value="row.done"></p>
      </div>
      <div class="cell">
        <p data-bind-value="row.date"></p>
      </div>
    </div>
  `;
}

function style(): string {
  return `
    <style>
      :host {
        display: contents;
      }
      .row {
        display: table-row;
      }
      ${cellStyle()}
    </style>
  `;
}
