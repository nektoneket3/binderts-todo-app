import { combineLatest, merge, of } from 'rxjs';
import { first, map, switchMap, delay } from 'rxjs/operators';
import { Bindable, getBindableDecorators, input, Input, component, Output, output } from 'binderts';

import { TodoFormCreatorService } from '../services/todo-form-creator.service';
import { FormModel } from '../models/form.model';
import { FormInputModel } from '../models/form-input.model';
import { TodoModel } from '../models/todo.model';


type RowIndex = number;
type EditMode = boolean;
type ChangedOutput = [RowIndex, TodoModel];

const { bindable, subscribedBindable } = getBindableDecorators<EditableTableRowComponent>();

@component({
  name: 'todo-editable-table-row',
  template: template(),
  style: style(),
  dependencies: ['TodoFormCreatorService']
})
export class EditableTableRowComponent {

  @input({ name: 'index' }) rowIndex!: Input<RowIndex>;
  @input() model!: Input<TodoModel>;
  @input({ name: 'edit-mode' }) editMode!: Input<EditMode>;

  @output() changed!: Output<ChangedOutput>;
  @output('edit-mode-changed') editModeChanged!: Output<[RowIndex, EditMode]>;

  @bindable<EditMode>(self => self.editMode.pipe(delay(100)))
  editModeAnimate!: Bindable<EditMode>;

  @bindable<string>((self) => self.model.pipe(
    first(),
    switchMap(({ done }) => merge(
      of(done ? 'green' : 'red'),
      of('').pipe(delay(500)),
    )),
  ))
  newRowBackgroundColor!: Bindable<string>;

  @bindable<TodoModel>(self => self.model.pipe(first(), map(model => ({ ...model }))))
  row!: Bindable<TodoModel>;

  @bindable<FormModel>(self => self.todoFormCreator.create(self.model))
  form!: Bindable<FormModel>;

  @subscribedBindable<[FormInputModel, TodoModel]>(
    self => self.row,
    ([{ name, value }, row], self) => self.row.next({ ...row, [name]: value })
  )
  formChanged!: Bindable<[FormInputModel, TodoModel]>;

  @subscribedBindable<[TodoModel, [RowIndex, EditMode]]>(
    self => combineLatest([self.rowIndex, self.editMode]),
    ([, [rowIndex, editMode]], self) => self.editModeChanged.next([rowIndex, !editMode])
  )
  rowClicked!: Bindable<[TodoModel, EditMode]>;

  @subscribedBindable<[Event, ChangedOutput]>(
    self => combineLatest([self.rowIndex, self.row]),
    ([, [rowIndex, row]], self) => self.changed.next([rowIndex, row])
  )
  updateButtonClicked!: Bindable<[Event, ChangedOutput]>;

  @subscribedBindable<[Event, ChangedOutput]>(
    self => combineLatest([self.rowIndex, self.row]),
    ([, [rowIndex, row]], self) => self.changed.next([rowIndex, { ...row, removed: true }])
  )
  deleteButtonClicked!: Bindable<[Event, ChangedOutput]>;

  constructor(
    private todoFormCreator: TodoFormCreatorService
  ) {}
}

function template(): string {
  return `
    <div class="container"
         data-bind-class="{ 'edit-mode': 'editMode', 'edit-mode-popup': 'editModeAnimate' }"
         data-bind-class-name="newRowBackgroundColor">

      <todo-table-row data-bind-model="model"
                      data-bind-clicked="rowClicked">
      </todo-table-row>
      
      <div class="edit-section"
           data-bind-if="editMode">
        <todo-form no-label
                   data-bind-model="form"
                   data-bind-changed="formChanged">
        </todo-form>
        <div class="buttons-container">
        
          <todo-button data-bind-label="'Save'"
                       data-bind-clicked="updateButtonClicked">
          </todo-button>
          <img alt=""
               title="Save"
               class="button-icon save-icon"
               src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADQAAAA0CAYAAADFeBvrAAAABmJLR0QA/wD/AP+gvaeTAAABdElEQVRoge2aT07CQBSHv3YBCUvdaIBT6TEMJ8KdHkG8AcqqxgWJ8RZCdFkXpag4r52pDX0d35e8Vfsyv6/Qf9OBL4bADHgEtkCupN6BB+CCAMbAk4LwdXUNjOpkhp4yd8Ak5Cj9kQmwcORYASdVjTNHk6uOKVMyFbI8A2dS00poOqy28R1H2u+FQvgXm4omzUI58AqchzRoFyr/fqchDdqFygvFKKRBu1AOzAGSgLBJ8+xOpHEPxwk5mJexCS1jE/qITYi0WRa9mJB2TEg7IUJtvz4kQn3H+TRdh++jxaLpAA2ZAvcB+XIgD7kP9YJ/fQ71AhPSTnRCIF8Cjz0P54s0X7d/45U2aJQpkebrKu9Dbb//tI0zd3TnkAlpx4S0Y0LaMSHtmJB2TEg7JqQdE9JOSrHyKhbeUmDddYoWWafAjbBR+ySJi1solpdldP+1wRfpq0QGDMqdxril+lLZzuEHA+CKYjmk7wqtLmsDLHeZ97/MJ7CHmP8UEGCRAAAAAElFTkSuQmCC"
               data-bind-click="updateButtonClicked"/>
          
          <todo-button data-bind-label="'Delete'"
                       data-bind-clicked="deleteButtonClicked">
          </todo-button>
          <img alt=""
               title="Delete"
               class="button-icon"
               src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAABmJLR0QA/wD/AP+gvaeTAAAA2UlEQVRIie2UvQ3CMBCFv1AANYUlZsgIgJQWMREdJfOAmCBQQccG0LEAVQhFDsk4ZzAmBQKedIruOX7Pd/6BX8EMKJ2Yx4ptFLHQyF2xlmJwiV2ZmHwZEg8fW2pNT9uDRhFiMABGCp95+DuEtChROB//mS36G7yNKdAHetpgE6foIZpq0RrYAUZyI3ntdb3BfoI1zq1mJ9weSOVbAtsQg4zqxroGQ+t/Y4kWlpnBg6MiqMUS6Mic1BIvJPdiDByeiC+AdmwFr8K7B0FHLQA50AUmwIlq5SvgfAXJvV8ICi52JAAAAABJRU5ErkJggg=="
               data-bind-click="deleteButtonClicked"/>
        
        </div>
      </div>
    </div>
  `;
}

function style(): string {
  return `
    <style>
      :host {
        display: contents;
        position: relative;
      }
      .container {
        display: table-row-group;
        transition: all .1s ease-in-out, background-color 1.75s ease;
        cursor: pointer;
      }
      .edit-mode.container {
        transform: scale(1.02);
      }
      .green {
        background-color: rgba(178,255,202,0.16);
      }
      .red {
        background-color: rgba(255,0,26,0.05);
      }
      .edit-section {
        transition: all .15s ease-in-out;
        z-index: 10;
        position: absolute;
        display: flex;
        flex-direction: row;
        justify-content: space-around;
        border: 1px solid;
        background: darkgrey;
        top: 0;
        transform: translateY(-90%);
        width: -moz-available;
        width: -webkit-fill-available;
        width: fill-available;
        opacity: 0;
        cursor: default;
        box-shadow: 0 0 14px -6px black;
      }
      .edit-mode-popup .edit-section {
        opacity: 1;
      }
      .edit-section todo-form {
        margin: 10px;
      }
      .buttons-container {
        display: flex;
        align-items: center;
      }
      todo-button {
        margin: 0 10px;
      }
      .button-icon {
        width: 23px;
        margin-right: 10px;
        display: none;
      }
      .save-icon {
        width: 20px;
      }
      todo-button, .button-icon {
        transition: all .15s ease-in-out;
      } 
      .button-icon:hover {
        transform: scale(1.25);
      }
      todo-button:hover {
        transform: scale(1.1);
      }
      @media screen and (max-width: 1000px) {
        todo-button {
          display: none;
        }
        .button-icon {
          cursor: pointer;
          display: initial;
        }
      }
    </style>
  `;
}
