import { map } from 'rxjs/operators';
import { input, Input, component, getBindableDecorators, Bindable } from 'binderts';


const { bindable } = getBindableDecorators<TableCaptionComponent>();

@component({
  name: 'todo-table-caption',
  template: template(),
  style: style(),
})
export class TableCaptionComponent {

  @input() count!: Input<number>;

  @bindable<string>(self => self.count.pipe(map(count => count === 1 ? '' : 's')))
  pluralEnding!: Bindable<string>;
}

function template(): string {
  return `
    <div class="table-caption">
      <span class="padding-right-5">You have</span>
      <todo-badge data-bind-add-class="'blue'"
                  data-bind-count="count">
      </todo-badge>
      TODO<span data-bind-value="pluralEnding"></span>:
    </div>
  `;
}

function style(): string {
  return `
    <style>
      .table-caption {
        margin-top: 15px;
        font-weight: 600;
        margin-bottom: 5px;
      }
      .padding-right-5 {
        padding-right: 5px;
      }
    </style>
  `;
}
