import { first } from 'rxjs/operators';
import { Bindable, getBindableDecorators, component, input, Input, output, Output } from 'binderts';

import { FormInputModel } from '../models/form-input.model';


type ChangedInput = unknown;

const { bindable, subscribedBindable } = getBindableDecorators<InputComponent>();

@component({
  name: 'todo-input',
  template: template(),
  style: style()
})
export class InputComponent {

  @input() model!: Input<FormInputModel>;

  @output() changed!: Output<FormInputModel>;

  @bindable<FormInputModel>(self => self.model.pipe(first()))
  initialModel!: Bindable<FormInputModel>;

  @subscribedBindable<[ChangedInput, FormInputModel]>(
    self => self.initialModel,
    ([value, initialModel], self) => self.changed.next({ ...initialModel, value })
  )
  inputChanged!: Bindable<[ChangedInput, FormInputModel]>;
}

function template(): string {
  return `
    <input data-bind-type="model.type"
           data-bind-input="model.value"
           data-bind-change="inputChanged">
  `;
}

function style(): string {
  return `
    <style>
      input {
        padding: 5px 15px 5px 15px;
      }
      input[type="text"] {
        width: 85%;
      }
    </style>
  `;
}
