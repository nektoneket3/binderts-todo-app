import { BehaviorSubject, from, fromEvent } from 'rxjs';
import { concatMap, map, toArray, withLatestFrom, distinctUntilChanged, filter } from 'rxjs/operators';
import { input, Input, component, output, Output, getBindableDecorators, Bindable, VIEW, OnViewConnected } from 'binderts';

import { TodoModel } from '../models/todo.model';


type RowIndex = number;
type EditMode = Bindable<boolean>;
type EditModes = EditMode[];

const { bindable, subscribedBindable } = getBindableDecorators<TableComponent>();

@component({
  name: 'todo-table',
  template: template(),
  style: style(),
  dependencies: [VIEW]
})
export class TableComponent implements OnViewConnected {

  @input() rows!: Input<Bindable<TodoModel>[]>;

  @output() updated!: Output<[RowIndex, TodoModel]>;

  @bindable<EditModes>((self) => self.rows.pipe(
    concatMap((rows) => from(rows).pipe(
      map(() => new BehaviorSubject(false).pipe(distinctUntilChanged()) as EditMode),
      toArray()
    )),
  ))
  editModes!: Bindable<EditModes>;

  @bindable<[Event, EditModes]>(
    ({ view, editModes }) => fromEvent(document, 'click').pipe(
      filter(event => !event.composedPath().map((element: any) => element.localName).includes(view.localName)),
      withLatestFrom(editModes)
    ),
    ([, editModes], self) => self.hideEditBoxes(editModes)
  )
  outsideClick!: Bindable<[Event, EditModes]>;

  @subscribedBindable<[[RowIndex, boolean], EditModes]>(
    self => self.editModes,
    ([[rowIndex, mode], editModes], self) => {
      self.hideEditBoxes(editModes, rowIndex);
      editModes[rowIndex].next(mode);
    }
  )
  modeChanged!: Bindable<[[RowIndex, boolean], EditModes]>;

  // @subscribedBindable((connected, self) => self.view.shadowRoot?.lastElementChild?.scrollIntoView(false))
  @subscribedBindable((connected, self) => ((self.view.shadowRoot || {}).lastElementChild || {} as any).scrollIntoView(false))
  onViewConnected!: Bindable<void>;

  constructor(
    private view: HTMLElement
  ) {}

  private hideEditBoxes(editModes: EditModes, rowIndex?: RowIndex): void {
    editModes.forEach((mode, index) => index !== rowIndex && mode.next(false));
  }
}

function template(): string {
  return `
    <div class="table">
      <div class="heading">
        <div class="cell">
            <p>Description</p>
        </div>
        <div class="cell">
            <p>Is done</p>
        </div>
        <div class="cell">
            <p>Created</p>
        </div>
      </div>
      
      <div class="row-container"
           data-bind-repeat="row in rows"
           data-bind-repeat-views-connected="onViewConnected">
        <todo-editable-table-row data-bind-if="!row.removed"
                                 data-bind-index="$$index"
                                 data-bind-model="row"
                                 data-bind-edit-mode="editModes[$$index]"
                                 data-bind-changed="updated"
                                 data-bind-edit-mode-changed="modeChanged">
        </todo-editable-table-row>
      </div>
    </div>
  `;
}

function style(): string {
  return `
    <style>
      :host {
        display: inline-block;
        overflow: auto;
        padding: 3px 15px;
        margin: 15px 0;
      }
      .table {
        display: table;
        position: relative;
        width: 100%;
        table-layout: fixed;
      }
      .heading {
        display: table-row;
        font-weight: bold;
        text-align: center;
      }
      .row-container {
        display: contents;
      }
      ${cellStyle()}
      
      /*@media screen and (max-width: 680px) {
        :host {
          overflow: auto;
          width: -moz-available;
          width: -webkit-fill-available;
          width: fill-available;
        } 
      }*/
    </style>
  `;
}

export function cellStyle(): string {
  return `
    .cell {
      display: table-cell;
      border: solid;
      border-width: thin;
      padding-left: 5px;
      padding-right: 5px;
      min-width: 190px;
      text-align: center;
    }
  `;
}
