import { Output, output, input, Input, component } from 'binderts';


@component({
  name: 'todo-button',
  template: template(),
  style: style(),
})
export class ButtonComponent {

  @input() label!: Input<string>;

  @output() clicked!: Output<Event>;
}

function template(): string {
  return `
    <button class="button"
            data-bind-click="clicked"
            data-bind-value="label">
    </button>
  `;
}

function style(): string {
  return `
    <style>
      .button {
        cursor: pointer;
        border: 1px solid cadetblue;
        border-radius: 15px;
        padding: 10px 15px;
        background-color: aliceblue;
        outline: none;
      }
    </style>
  `;
}
