import { merge, of } from 'rxjs';
import { skip, switchMap, distinctUntilChanged, delay } from 'rxjs/operators';
import { input, Input, component, getBindableDecorators, Bindable } from 'binderts';


const { bindable } = getBindableDecorators<BadgeComponent>();

@component({
  name: 'todo-badge',
  template: template(),
  style: style(),
})
export class BadgeComponent {

  @input() count!: Input<number>;
  @input({ name: 'add-class' }) addedClass!: Input<string>;

  @bindable<boolean>(({ count }) => count.pipe(
    distinctUntilChanged(),
    skip(1),
    switchMap(() => merge(
      of(true),
      of(false).pipe(delay(500))
    ))
  ))
  animate!: Bindable<boolean>;
}

function template(): string {
  return `
    <span class="container">
      <span class="badge"
            data-bind-value="count"
            data-bind-class="{ 'animate': 'animate' }"
            data-bind-class-name="addedClass">
      </span>
    </span>
  `;
}

function style(): string {
  return `
    <style>
      .container {
        display: inline-block;
        position: relative;
        width: 30px;
        height: 20px;
      }
      .badge {
        position: absolute;
        border-radius: 50px;
        border: 2px solid green;
        padding: 5px;
        background-color: #c7d4bb;
        transition: all .1s ease-in-out;
      }
      .badge.red {
        background-color: #ff8484;
        border: 2px solid #802a13;
      }
      .badge.blue {
        background-color: #b0daff;
        border: 2px solid #5a728e;
      }
      .badge.animate {
        transform: scale(1.3);
      }
    </style>
  `;
}
