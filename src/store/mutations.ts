import { BehaviorSubject, Observable } from 'rxjs';
import { withLatestFrom } from 'rxjs/operators';

import { State, defaultTodo } from './state';
import { TodoModel } from '../models/todo.model';


export const changeTodo = 'changeTodo';
export const createTodo = 'createTodo';
export const removeTodos = 'removeTodos';
export const updateTodo = 'updateTodo';
export const changeMode = 'changeMode';

export const mutations = {
  [changeTodo](state: State, payload: Observable<Partial<TodoModel>>) {
    payload.pipe(
      withLatestFrom(state.todo)
    ).subscribe(([partialTodo, currentTodo]) => {
      console.log('todo changed');
      state.todo.next({ ...currentTodo, ...partialTodo });
    });
  },
  [createTodo](state: State, payload: Observable<void>) {
    payload.pipe(
      withLatestFrom(state.todos, state.todo)
    ).subscribe(([, currentTodos, newTodo]) => {
      state.todos.next([...currentTodos, new BehaviorSubject<TodoModel>({ ...newTodo, date: new Date() })]);
      state.todo.next(defaultTodo);
      console.log('todo created');
    });
  },
  [removeTodos](state: State, payload: Observable<void>) {
    payload.subscribe(() => {
      state.todos.next([]);
      console.log('todos removed');
    });
  },
  [updateTodo](state: State, payload: Observable<[number, TodoModel]>) {
    payload.pipe(
      withLatestFrom(state.todos)
    ).subscribe(([[index, updatedTodo], currentTodos]) => {
      currentTodos[index].next(updatedTodo);
      console.log('todo updated');
    });
  },
  [changeMode](state: State, payload: Observable<void>) {
    payload.pipe(
      withLatestFrom(state.darkMode)
    ).subscribe(([, darkMode]) => {
      state.darkMode.next(!darkMode);
      console.log('darkMode changed');
    });
  }
};
