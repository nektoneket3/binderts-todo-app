import { StoreParams } from 'binderts';

import { state, State } from './state';
import { mutations } from './mutations';


const store: StoreParams<State> = {
  state,
  mutations
};

export { store, State };
