import { BehaviorSubject } from 'rxjs';

import { TodoModel } from '../models/todo.model';


export interface State {
  todo: BehaviorSubject<TodoModel>;
  todos: BehaviorSubject<BehaviorSubject<TodoModel>[]>;
  darkMode: BehaviorSubject<boolean>;
}

export const defaultTodo: TodoModel = {
  done: false,
  description: 'Test Description',
  date: null,
  removed: false
};

export const state: State = {
  todo: new BehaviorSubject<TodoModel>(defaultTodo),
  todos: new BehaviorSubject<BehaviorSubject<TodoModel>[]>([]),
  darkMode: new BehaviorSubject<boolean>(false),
};
